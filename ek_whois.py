#-*- coding:utf8 -*-
import whois #whois module importing
from Tkinter import * # tkinter module importing
from tkMessageBox import *
import re

class application(object):
	def __init__(self):
		self.tk = Tk() #set window
		self.tk.resizable(0,0)
		self.tk.tk_setPalette("#000000") #set form bg
		window_width = 400 #set form width
		window_height = 200 #set form height
		self.tk.title("Whois Application") #set title
		display_width = self.tk.winfo_screenwidth() #set screen width
		display_height = self.tk.winfo_screenheight() #set screen height
		x = (display_width - window_width) / 2
		y = (display_height - window_height) / 2
		self.tk.geometry("%dx%d+%d+%d"%(window_width, window_height, x, y))
		frm_row = Frame(height=10)
		frm_row.grid(row=1,column=1)
		lbl = Label(text = u"Site Adı", fg="green")
		lbl.grid(row=2,column=1)
		frm_col = Frame(width=15)
		frm_col.grid(row=2,column=2)
		self.input = Entry(width=40, borderwidth=1, fg="green")
		self.input.insert(0, "http://")
		self.input.grid(row=2,column=3)
		frm_colpad = Frame(width=15)
		frm_colpad.grid(row=2,column=4)
		btn = Button(text=u"Çalıştır", bg="#000000", fg="green", command=self.run)
		btn.grid(row=2, column=5)
		mainloop() # generate
	def run(self):
		site = self.input.get()
		url = re.search('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', site)
		if url:
			w = whois.whois(site)
			scrl = Scrollbar(self.tk)
			scrl.grid(row=5, column=2)
			text= Text(fg = "green", font="Verdana 8", height=10, width=55, yscrollcommand=scrl.set)
			text.insert(END, u"%s"%(w.text))
			frm_line = Frame(height=10)
			frm_line.grid(row=3, column=1)
			text.grid(row=5)
			scrl.config(command=text.yview)
			mainloop() # generate
		else:
			showerror("Hata!", u"Lütfen geçerli bir URL girin!")
		
app = application() #instance class